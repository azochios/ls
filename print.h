#ifndef PRINT_H 
#define PRINT_H

#include <sys/stat.h>
#include <sys/types.h>

#include <fts.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

/* The st_mode does not exceed 11 characters */
#define MODE_LEN 11
/* Likewise, human readable size is up to 6 characters 
 * %ddd.%d[B,K,M,G,...]
 * */
#define BYTE_SIZE_LEN    6
#define TIME_LEN	15

void print(FTSENT * ftsent);
void print_time(const time_t* clock, char* time_buf);

#endif /* PRINT_H */

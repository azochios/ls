# CFLAGS = -Wall -Werror -Wextra -g
CFLAGS = -g
ls :	ls.o cmp.o print.o
	 cc $(CFLAGS) -o ls cmp.o ls.o print.o

ls.o : ls.c ls.h
	 cc $(CFLAGS) -c $< 

cmp.o : cmp.c cmp.h
	 cc $(CFLAGS) -c $< 

print.o : print.c print.h
	 cc $(CFLAGS) -c $< 

clean : 
	rm ls cmp.o ls.o print.o

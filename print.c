#include "ls.h"
#include "print.h"

void
print(FTSENT * ftsent)
{

	if (lflag || nflag) {
		mode_t m = ftsent->fts_statp->st_mode;
		char bp[MODE_LEN]; 
		strmode(m, bp);
		printf("%s ", bp);
		printf("%d ", ftsent->fts_statp->st_nlink);

		if (lflag) {
			printf("%d ", ftsent->fts_statp->st_uid);
			printf("%d ", ftsent->fts_statp->st_gid);
		} else {
			printf("%d ", ftsent->fts_statp->st_uid);
			printf("%d ", ftsent->fts_statp->st_gid);
		}

		if (hflag) {
			char bytes[BYTE_SIZE_LEN];
			humanize_number(bytes, BYTE_SIZE_LEN, ftsent->fts_statp->st_size, " ",  HN_AUTOSCALE, HN_NOSPACE);
			printf("%s ", bytes);
		} else {
			printf("%d ", ftsent->fts_statp->st_size);
		}

		char time_buf[TIME_LEN];
		time_t clock;

		if (cflag){
			clock = ftsent->fts_statp->st_ctime;
			print_time(&clock, time_buf);
		} else if (uflag) {
			clock = ftsent->fts_statp->st_atime;
			print_time(&clock, time_buf);
		/* Use of st_mtime if above flags not set */
		} else {
			clock = ftsent->fts_statp->st_mtime;
			print_time(&clock, time_buf);
		}

		printf("%s ", time_buf);


	}
	if (dflag) {
		printf("%s\n", ftsent->fts_path);
	} else {
		printf("%s\n",  ftsent->fts_name);
	}
	if (alt_print) {
		struct stat* file_stat = ftsent->fts_statp;
		mode_t m = file_stat->st_mode;
		if (S_ISDIR(m)) {
			printf("%s\\\n",  ftsent->fts_name);
		}
	}
}

void print_time(const time_t* clock, char* time_buf) {
	struct tm * time;
	localtime_r(clock, time);
	strftime(time_buf, TIME_LEN, "%b %e %k:%M ", time);

}

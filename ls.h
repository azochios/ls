#ifndef LS_H 
#define LS_H 

#include <sys/stat.h>
#include <sys/types.h>

#include <fts.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>


extern int all_but_dots;	/* -A default for uid 0 */
extern int aflag;	 	
extern int cflag;		
extern int dflag;
extern int alt_print;		/* -F alternate printing */
extern int fflag;
extern int hflag;
extern int iflag;
extern int kflag;
extern int lflag;
extern int nflag;
extern int qflag;
extern int recurse;		/* -R traverse the hierarchy recursively */
extern int rflag;
extern int size_sort;		/* -S sort by file size (desc) */
extern int sflag;
extern int tflag;
extern int uflag;
extern int wflag;

#endif /* LS_H */

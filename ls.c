#include "cmp.h"
#include "ls.h"
#include "print.h"

int all_but_dots, aflag, cflag, dflag, alt_print, fflag, hflag, 
      	iflag, kflag, lflag, nflag, qflag, recurse, rflag, 
		size_sort, sflag, tflag, uflag, wflag;

FTSENT * fts_voyage(char* argvi[]);

int
main(int argc, char* argv[])
{
	int ch;
	
	setprogname(argv[0]);

	while ((ch = getopt(argc, argv, "AacdFfhiklnqRrSstuw")) != -1) {
		switch (ch) {
		case 'A':
			all_but_dots= 1;
			break;
		case 'a':
			aflag = 1;
			break;
		case 'c':
			cflag = 1;
			/* option override */
			uflag = 0;
			break;
		case 'd':
			dflag = 1;
			break;
		case 'F':
			alt_print = 1;
			break;
		case 'f':
			fflag = 1;
			break;
		case 'h':
			hflag = 1;
			/* option override */
			kflag = 0;
			break;
		case 'i':
			iflag = 1;
			break;
		case 'k':
			kflag = 1;
			/* option override */
			hflag = 0;
			break;
		case 'l':
			lflag = 1;
			/* option override */
			nflag = 0;
			break;
		case 'n':
			nflag = 1;
			/* option override */
			lflag = 0;
			break;
		case 'q':
			qflag = 1;
			/* option override */
			wflag = 0;
			break;
		case 'R':
			recurse = 1;
			break;
		case 'r':
			rflag = 1;
			break;
		case 'S':
			size_sort= 1;
			break;
		case 's':
			sflag = 1;
			break;
		case 't':
			tflag = 1;
			break;
		case 'u':
			uflag = 1;
			/* option override */
			cflag = 0;
			break;
		case 'w':
			wflag = 1;
			/* option override */
			qflag = 0;
			break;
		case '?':
		default:
			printf("usage: %s -AacdFfhiklnqRrSstuw\n", 
					getprogname());
			/* NOTREACHED */
		}
	}
	argc -= optind;
	argv += optind;
	
	fts_voyage(argv);

	exit(EXIT_SUCCESS);
}

FTSENT * 
fts_voyage(char* argv[]) 
{	
	int stirred_opts; 
	/* Make logical traverse default */
	if (!dflag) {
		stirred_opts = FTS_LOGICAL;
	} else {
		stirred_opts = FTS_PHYSICAL;
	
	}
	if (aflag) {
		stirred_opts |= FTS_SEEDOT;
	}
	/* 
	 * The following flags rely upon the struct stat.
	 * if not set, call fts_open without fetching 
	 * it for optimization reasons
	 */
	if (!cflag && !hflag && !iflag && !kflag && !lflag 
		&& !nflag && !rflag && !size_sort && !sflag 
		&& !tflag && !uflag) {
			stirred_opts |= FTS_NOSTAT;
	}

	FTS * ftsp;
	//FTSENT * parent;
	FTSENT * child;
	
	if (*argv == NULL) {
		*argv = ".";
	}
	if (!fflag) {
		ftsp = fts_open(argv, stirred_opts, NULL);
	} else {
		ftsp = fts_open(argv, stirred_opts, &comp);
	}
	for (argv; *argv != NULL; ++argv) {
		child = fts_read(ftsp);
		print(child);
	}

	fts_close(ftsp);
	return(NULL);	
}
/*
if(!dflag) {
			child = fts_children(ftsp, 0);
		}
		while (child != NULL) {
			if (!aflag && *child->fts_name == '.') {
				child = child->fts_link;
				continue;
			}
			print(child);
			child = child->fts_link;
		}
		fts_close(ftsp);
*/

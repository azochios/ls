#include "cmp.h"
#include "ls.h"
int  
comp(const FTSENT ** a, const FTSENT ** b)
{
	if (tflag) {
		if (cflag) {
			time_t time_a = (*a)->fts_statp->st_ctime;
			time_t time_b = (*b)->fts_statp->st_ctime;				
			if (!rflag) {
				return(sort_by_time(time_a, time_b));
			} else {
				return(sort_by_time(time_b, time_a));
			}
		} else if (uflag) {
			time_t time_a = (*a)->fts_statp->st_atime;
			time_t time_b = (*b)->fts_statp->st_atime;

			if (!rflag) {
				return(sort_by_time(time_a, time_b));
			} else {
				return(sort_by_time(time_b, time_a));
			}
		} else {
			time_t time_a = (*a)->fts_statp->st_mtime;
			time_t time_b = (*b)->fts_statp->st_mtime;

			if (!rflag) {
				return(sort_by_time(time_a, time_b));
			} else {
				return(sort_by_time(time_b, time_a));
			}

		}
	} else if (size_sort) {
			off_t size_a = (*a)->fts_statp->st_size;
			off_t size_b = (*b)->fts_statp->st_size;

			if (!rflag) {
				return(sort_by_size(size_a, size_b));
			} else {
				return(sort_by_size(size_b, size_a));
			}

	}
	/* Default behavior; sort by type, then by name */
	else {
		if (!rflag) {
			return(sort_by_default(a, b));
		} else {
			return(sort_by_default(b, a));
		}
	}
	return(0);
}

int
sort_by_name(const FTSENT ** a, const FTSENT ** b)
{
	int len_a = (*a)->fts_pathlen;
	int len_b = (*b)->fts_pathlen;
	int len = len_a >= -len_b? len_a : len_b;
	return(-strncmp((*a)->fts_name, (*b)->fts_name, len));
}

/* Three-way comparison for types time_t and off_t. Negative value
 * implies precedence. 
*/
int
sort_by_time(time_t a, time_t b) {
	if (a > b) {
		return(-1);
	} else if (a == b) {
		return(0);
	} else {
		return(1);
	}
}
int
sort_by_size(off_t a, off_t b) {
	if (a > b) {
		return(-1);
	} else if (a == b) {
		return(0);
	} else {
		return(1);
	}
}
int
sort_by_default(const FTSENT ** a, const FTSENT ** b)
{
	/* Use shorter pathname for lexicographical sorting */
	/*int len = (*a)->fts_pathlen <= (*b)->fts_pathlen ? 
		(*a)->fts_pathlen : (*b)->fts_pathlen;
	*/
	/* FTS_D = 1; preserve order of arguments */
	if ((*a)->fts_info >= (*b)->fts_info) {
		return(strcmp((*a)->fts_name, (*b)->fts_name));
	}
	else {
		return(strcmp((*b)->fts_name, (*a)->fts_name));
	}
}

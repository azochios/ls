#ifndef COMP_H 
#define COMP_H 

#include <sys/stat.h>
#include <sys/types.h>
#include <fts.h>
#include <string.h>

int comp(const FTSENT ** a, const FTSENT ** b);

int sort_by_name(const FTSENT ** a, const FTSENT ** b);
int sort_by_time(time_t a, time_t b);
int sort_by_size(off_t a, off_t b);
int sort_by_default(const FTSENT ** a, const FTSENT ** b);
#endif /* COMP_H */
